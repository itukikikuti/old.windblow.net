<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Panel.php");

class Game extends Panel {
    private $id;
    private $width;
    private $height;
    private $mwidth;
    private $mheight;
    
    function __construct($id, $width, $height, $mwidth, $mheight) {
        $this->id = $id;
        $this->width = $width;
        $this->height = $height;
        $this->mwidth = $mwidth;
        $this->mheight = $mheight;
        $this->EchoHTML();
    }

    protected function EchoHTML() {
        echo $this->GetHTMLWithNoIndentFormat("
        <script src='/file/UnityLoader.js'></script>
        <script>var gameInstance = UnityLoader.instantiate('gameContainer', '/file/$this->id/$this->id.json');</script>
        <div class='panel game' id='gameContainer' %s></div>", $this->GetDataAttribute($this->width, $this->height, $this->mwidth, $this->mheight));
    }
}
?>
