<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Link.php");

class GameFullScreen extends Link {
    function __construct() {
        parent::__construct("", "javascript:gameInstance.SetFullscreen(1)", "/image/icon/fullscreen.jpg", 1, 1, false, false);
    }
}
?>
