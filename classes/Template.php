<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Panel.php");

class Template extends Panel {
    private $title;

    function __construct($title) {
        $this->title = $title;
        $this->EchoHTML();
    }

    function __destruct() {
        echo $this->GetHTMLWithNoIndentFormat("
        <div style='clear:both;'></div>
        </body>
        </html>
        ");
    }

    protected function EchoHTML() {
        $login = $_SERVER["HTTP_HOST"] == "localhost" || $_SERVER["REMOTE_ADDR"] == "124.154.11.143";
        echo $this->GetHTMLWithNoIndentFormat("
        <!DOCTYPE html>
        <html lang='en'>
        <head>
        <meta charset='utf-8'>");
        if ($login) {
            echo $this->GetHTMLWithNoIndentFormat("
            <link rel='icon' type='image/png' href='/photos/icon/login.png'>");
        }
        else {
            echo $this->GetHTMLWithNoIndentFormat("
            <link rel='icon' type='image/png' href='/photos/icon/favicon.png'>");
        }
        echo $this->GetHTMLWithNoIndentFormat("
        <script type='text/javascript' src='/style.js'></script>
        <link rel='stylesheet' type='text/css' href='/normalize.css'>
        <link rel='stylesheet' type='text/css' href='/style.css'>
        <title>".$this->title."windblow</title>
        </head>
        <body>");
        if ($login) {
            return;
        }            
        echo $this->GetHTMLWithNoIndentFormat("
        <style>.async-hide { opacity: 0 !important} </style>
        <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
        h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
        (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
        })(window,document.documentElement,'async-hide','dataLayer',4000,
        {'GTM-M5DZSLX':true});</script>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-104237160-1', 'auto');
        ga('require', 'GTM-M5DZSLX');
        ga('send', 'pageview');
        </script>");
    }
}
?>
