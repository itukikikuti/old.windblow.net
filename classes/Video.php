<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Panel.php");

class Video extends Panel {
    private $id;
    private $width;
    private $height;
    private $mwidth;
    private $mheight;
    
    function __construct($id, $size, $msize) {
        $this->id = $id;
        $this->width = $size;
        $this->height = ceil($size * 0.5625);
        $this->mwidth = $msize;
        $this->mheight = ceil($msize * 0.5625);
        $this->EchoHTML();
    }

    protected function EchoHTML() {
        echo $this->GetHTMLWithNoIndentFormat("
        <div class='panel video' %s>
        <iframe src='https://www.youtube.com/embed/$this->id' frameborder='0' allowfullscreen></iframe>
        </div>", $this->GetDataAttribute($this->width, $this->height, $this->mwidth, $this->mheight));
    }
}
?>
