<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Panel.php");

class Space extends Panel {
    private $width;
    private $height;
    private $mwidth;
    private $mheight;
    
    function __construct($width, $height, $mwidth, $mheight) {
        $this->width = $width;
        $this->height = $height;
        $this->mwidth = $mwidth;
        $this->mheight = $mheight;
        $this->EchoHTML();
    }

    protected function EchoHTML() {
        echo $this->GetHTMLWithNoIndentFormat("
        <div class='panel space' %s></div>", $this->GetDataAttribute($this->width, $this->height, $this->mwidth, $this->mheight));
    }
}
?>
