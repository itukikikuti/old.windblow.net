<?php
abstract class Panel {
    abstract protected function EchoHTML();

    public function GetHTMLWithNoIndentFormat($html, $attribute = "") {
        return str_replace("  ", "", sprintf($html, $attribute));
    }

    public function GetDataAttribute($width, $height, $mwidth, $mheight) {
        return "data-width='$width' data-height='$height' data-mwidth='$mwidth' data-mheight='$mheight'";
    }
}
?>
