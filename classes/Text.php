<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Panel.php");

class Text extends Panel {
    private $text;
    private $width;
    private $height;
    private $mwidth;
    private $mheight;
    private $fontsize;
    private $textalign;
    private $overflow;
    
    function __construct($text, $width, $height, $mwidth, $mheight, $fontsize, $isCenter, $isScroll) {
        $this->text = $text;
        $this->width = $width;
        $this->height = $height;
        $this->mwidth = $mwidth;
        $this->mheight = $mheight;
        $this->fontsize = $fontsize;
        if ($isCenter) {
            $this->textalign = "center";
        }
        else {
            $this->textalign = "left";
        }
        if ($isScroll) {
            $this->overflow = "scroll";
        }
        else {
            $this->overflow = "visible";
        }
        $this->EchoHTML();
    }

    protected function EchoHTML() {
        echo $this->GetHTMLWithNoIndentFormat("
        <div class='panel text' %s>
        $this->text
        </div>", $this->GetDataAttribute($this->fontsize, $this->textalign, $this->overflow, $this->width, $this->height, $this->mwidth, $this->mheight));
    }

    public function GetDataAttribute($fontsize, $textalign, $overflow, $width, $height, $mwidth, $mheight) {
        return "data-fontsize='$fontsize' data-textalign='$textalign' data-overflow='$overflow'".parent::GetDataAttribute($width, $height, $mwidth, $mheight);
    }
}
?>
