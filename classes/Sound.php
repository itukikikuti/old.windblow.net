<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Panel.php");

class Sound extends Panel {
    private $id;
    private $width;
    private $height;
    private $mwidth;
    private $mheight;
    
    function __construct($id, $size, $msize) {
        $this->id = $id;
        $this->width = $size;
        $this->height = 2;
        $this->mwidth = $msize;
        $this->mheight = 1;
        $this->EchoHTML();
    }

    protected function EchoHTML() {
        echo $this->GetHTMLWithNoIndentFormat("
        <div class='panel sound' %s>
        <iframe src='https://w.soundcloud.com/player/?url=https://api.soundcloud.com/tracks/$this->id&auto_play=true' scrolling='no' frameborder='0'></iframe>
        </div>", $this->GetDataAttribute($this->width, $this->height, $this->mwidth, $this->mheight));
    }
}
?>
