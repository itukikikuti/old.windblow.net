<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Panel.php");

class Link extends Panel {
    private $title;
    private $link;
    private $photo;
    private $size;
    private $msize;
    private $target;
    private $label;
    
    function __construct($title, $link, $photo, $size, $msize, $isThumbnail, $isOutside, $isLabel = false) {
        $this->title = $title;
        $this->link = $link;
        if ($isThumbnail) {
            $this->photo = $this->GetThumbnailPath($photo, 128 * $size);
        }
        else {
            $this->photo = $photo;
        }
        $this->size = $size;
        $this->msize = $msize;
        if ($isOutside) {
            $this->target = "_blank";
        }
        else {
            $this->target = "_self";
        }
        if ($isLabel) {
            $this->label = "<p>$this->title</p>";
        }
        else {
            $this->label = "";
        }            
        $this->EchoHTML();
    }

    protected function EchoHTML() {
        echo $this->GetHTMLWithNoIndentFormat("
        <a class='panel link' href='$this->link' target='$this->target' onmouseover='OnMouseOver(this)' onmouseout='OnMouseOut(this)' %s>
        <img src='$this->photo' alt='$this->title'>$this->label
        </a>", $this->GetDataAttribute($this->size, $this->size, $this->msize, $this->msize));
    }

    private function GetThumbnailPath($url, $size) {
        $thum = $_SERVER["DOCUMENT_ROOT"]."/temp".$url.$size."temp.jpg";
        $url = $_SERVER["DOCUMENT_ROOT"].$url;

        if (is_file($thum)) {
            return str_replace($_SERVER["DOCUMENT_ROOT"], "", $thum);
        }

        $path = pathinfo($thum);
        if (!is_readable($path["dirname"])) {
            mkdir($path["dirname"], 0755, true);
        }

        list($width, $height) = getimagesize($url);
        if ($width > $height)
            $scale = $size / $height;
        else
            $scale = $size / $width;
        switch (mime_content_type($url)) {
            case "image/jpeg":
                $image = imagecreatefromjpeg($url);
                break;
            case "image/png":
                $image = imagecreatefrompng($url);
                break;
        }
        $w = $width * $scale;
        $h = $height * $scale;
        $canvas = imagecreatetruecolor($size, $size);
        imagecopyresampled($canvas, $image, ($size - $w) / 2, ($size - $h) / 2, 0, 0, $w, $h, $width, $height);
        imagejpeg($canvas, $thum, 90);
        imagedestroy($canvas);
        return str_replace($_SERVER["DOCUMENT_ROOT"], "", $thum);
    }
}
?>
