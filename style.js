window.onload = function() {
    if (window.innerWidth > window.innerHeight) {
        var size = 64;
        var p = document.getElementsByClassName("panel");
        for (var i = 0; i < p.length; i++) {
            p[i].style.width = (size * p[i].dataset.width - 1) + "px";
            p[i].style.height = (size * p[i].dataset.height - 1) + "px";
        }
        var t = document.getElementsByClassName("text");
        for (var i = 0; i < t.length; i++) {
            t[i].style.fontSize = t[i].dataset.fontsize + "px";
            t[i].style.textAlign = t[i].dataset.textalign;
            t[i].style.overflow = t[i].dataset.overflow;
            if (t[i].style.overflow == "scroll") {
                t[i].style.boxShadow = "4px 4px 16px rgba(0, 0, 0, 0.25) inset";
                t[i].style.padding = "16px";
                t[i].style.width = (size * t[i].dataset.width - 1 - 32) + "px";
                t[i].style.height = (size * t[i].dataset.height - 1 - 32) + "px";
            }
        }
    }
    else {
        document.body.style.width = "calc(100% - 20px)";
        document.body.style.margin = "100px 10px";
        var size = Math.floor((window.innerWidth - 20 - 6) / 6);
        var p = document.getElementsByClassName("panel");
        for (var i = 0; i < p.length; i++) {
            if (p[i].dataset.mwidth == 0 || p[i].dataset.mheight == 0) {
                p[i].remove();
                i--;
                continue;
            }
            p[i].style.width = (size * p[i].dataset.mwidth - 1) + "px";
            p[i].style.height = (size * p[i].dataset.mheight - 1) + "px";
        }
        var t = document.getElementsByClassName("text");
        for (var i = 0; i < t.length; i++) {
            t[i].style.textAlign = t[i].dataset.textalign;
            t[i].style.overflow = t[i].dataset.overflow;
            if (t[i].style.overflow == "scroll") {
                t[i].style.boxShadow = "4px 4px 16px 0px rgba(0, 0, 0, 0.25) inset";
                t[i].style.fontSize = Math.round(size * 0.016 * t[i].dataset.fontsize) + "px";
                t[i].style.padding = "16px";
                t[i].style.width = (size * t[i].dataset.mwidth - 1 - 32) + "px";
                t[i].style.height = (size * t[i].dataset.mheight - 1 - 32) + "px";
            }
            else {
                var scale = (t[i].dataset.mwidth * t[i].dataset.mheight) / (t[i].dataset.width * t[i].dataset.height);
                t[i].style.fontSize = Math.round(size * 0.016 * t[i].dataset.fontsize * scale) + "px";
            }
        }
    }
}

function OnMouseOver(e) {
    e.style.zIndex = "1";
}

function OnMouseOut(e) {
    e.style.zIndex = "0";
}
