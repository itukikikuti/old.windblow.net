<?php
if (php_sapi_name() !== "cli") {
    echo "Not a command line";
    return;
}

$ftp = new FTP("windblow.net");
$ftp->Sync();

fgets(STDIN);

class FTP {
    private $stream;
    private $rewList;
    private $username;
    private $time;

    function __construct($username) {
        $this->username = $username;
        $this->time = microtime(true);
    }

    function __destruct() {
        if ($this->stream !== null) {
            ftp_close($this->Get());
            echo "Close ".(microtime(true) - $this->time)."sec \n";
        }
    }

    function Sync() {
        $this->Remove(".");
        $this->Generate(".");
    }

    function Generate($path) {
        $local = $this->GetPathList($path, false);
        $remote = $this->GetPathList($path, true);

        for ($i = 0; $i < count($local); $i++) {
            if ($this->IsFile($local[$i], false)) {
                if (in_array($local[$i], $remote, true) === false ||
                    $this->IsFileNeedUpdate($local[$i])) {
                    $this->UploadFile($local[$i]);
                }
            }
            if ($this->IsDirectory($local[$i], false)) {
                if (in_array($local[$i], $remote, true) === false) {
                    $this->MakeDirectory($local[$i]);
                }
                $this->Generate($local[$i]);
            }
        }
    }

    function Remove($path) {
        $local = $this->GetPathList($path, false);
        $remote = $this->GetPathList($path, true);
        
        for ($i = 0; $i < count($remote); $i++) {
            if ($this->IsFile($remote[$i], true)) {
                if (in_array($remote[$i], $local, true) === false) {
                    $this->DeleteFile($remote[$i]);
                }
            }
            if ($this->IsDirectory($remote[$i], true)) {
                $this->Remove($remote[$i]);
                if (in_array($remote[$i], $local, true) === false) {
                    $this->DeleteDirectory($remote[$i]);
                }
            }
        }
    }

    function GetPathList($path, $isRemote) {
        $list = $this->ScanDirectory($path, $isRemote);
        $pathList = array();
        for ($i = 0; $i < count($list); $i++) {
            if ($this->GetName($list[$i]) === "." ||
                $this->GetName($list[$i]) === "..")
                continue;
            $pathList[] = $list[$i];
        }
        return $pathList;
    }

    function ScanDirectory($path, $isRemote) {
        if ($isRemote) {
            $list = ftp_nlist($this->Get(), $path);
            if (!$list) {
                return array();
            }
            for ($i = 0; $i < count($list); $i++) {
                $list[$i] = "./".$list[$i];
            }
            return $list;
        }
        else {
            $list = @scandir($path);
            if (!$list) {
                return array();
            }
            for ($i = 0; $i < count($list); $i++) {
                $list[$i] = $path."/".$list[$i];
            }
            return $list;
        }
    }

    function IsDirectory($path, $isRemote) {
        if ($isRemote) {
            $list = $this->GetRawList($path);
            for ($i = 0; $i < count($list); $i++) {
                if (strpos($list[$i], $this->GetName($path)) === false) {
                    continue;
                }
                if (substr($list[$i], 0, 1) === "d") {
                    return true;
                }
            }
            return false;
        }
        else {
            return is_dir($path);
        }
    }

    function IsFile($path, $isRemote) {
        if ($isRemote) {
            $list = $this->GetRawList($path);
            for ($i = 0; $i < count($list); $i++) {
                if (strpos($list[$i], $this->GetName($path)) === false) {
                    continue;
                }
                if (substr($list[$i], 0, 1) === "-") {
                    return true;
                }
            }
            return false;
        }
        else {
            return is_file($path);
        }
    }

    function IsFileNeedUpdate($path) {
        $time = ftp_mdtm($this->Get(), $path);
        if ($time === -1) {
            return false;
        }
        return filemtime($path) > $time;
    }

    function MakeDirectory($path) {
        echo "Make directory ".$path;
        if (ftp_mkdir($this->Get(), $path) !== false) {
            echo " is done.\n";
        }
    }

    function UploadFile($path) {
        echo "Upload file ".$path;
        if (ftp_put($this->Get(), $path, $path, FTP_BINARY) !== false) {
            echo " is done.\n";
        }
    }

    function DeleteDirectory($path) {
        echo "Delete directory ".$path;
        if (ftp_rmdir($this->Get(), $path) !== false) {
            echo " is done.\n";
        }
    }

    function DeleteFile($path) {
        echo "Delete file ".$path;
        if (ftp_delete($this->Get(), $path) !== false) {
            echo " is done.\n";
        }
    }

    function GetName($path) {
        $pathInfo = pathinfo($path);
        return $pathInfo["basename"];
    }

    function GetPath($path) {
        $pathInfo = pathinfo($path);
        return $pathInfo["dirname"];
    }

    function GetRawList($path) {
        if ($this->rawList === null) {
            $this->rawList = array();
        }
        if (array_key_exists($this->GetPath($path), $this->rawList) === false) {
            $this->rawList[$this->GetPath($path)] = ftp_rawlist($this->Get(), $this->GetPath($path));
        }
        return $this->rawList[$this->GetPath($path)];
    }

    function Get() {
        if ($this->stream === null) {
            $this->stream = ftp_connect($this->username);
            echo "Connect"."\n";
            if (ftp_login($this->Get(), $this->username, $this->GetPassword())) {
                echo "Login"."\n";
            }
			fgets(STDIN);
        }
        return $this->stream;
    }

    function GetPassword() {
        echo "Password:";
        return str_replace("\n", "", fgets(STDIN));
    }
}
?>
