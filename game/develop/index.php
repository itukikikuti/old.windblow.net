<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Template.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Link.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Space.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Text.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Video.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Const.php");

$t = new Template("$gameDevelop | ");

new Link($windblow, "/", "/image/icon/windblow.jpg", 4, 4, false, false);
new Link($gameDevelop, "/game/develop/", "/image/icon/unity.jpg", 1, 2, false, false);
new Text(
    "<p>$gameDevelop</p>",
    7, 1, 2, 2, 26, false, false);
new Text(
    "<p>わかりやすく、ゆるく、ゲームを作れるようになる動画を公開してます！！</p>
    <p>ゲームを作って見たい人、ゲームクリエイタを目指している人はぜひご覧ください！！</p>",
    8, 2, 6, 2, 24, false, false);
new Space(8, 1, 0, 0);
new Link($installUnity, "/game/develop/install.php", "/image/develop/2.jpg", 6, 3, true, false, true);
new Link($shooter2D, "/game/develop/shooter-2d.php", "/image/develop/shooter2D.jpg", 6, 3, false, false, true);

new Text(
    "<p>9.7.2017 Add $installUnity のページ</p>
    <p>9.6.2017 Add $shooter2D のページ</p>
    <p>8.16.2017 Happy birthday, $gameDevelop!!!</p>",
    12, 2, 6, 2, 24, false, true);
$license = file_get_contents("LICENSE_OFL.txt");
$license = str_replace("\n", "<br>", $license);
new Text(
    "<h1>WebGLゲーム内使用フォントライセンス</h1>
    <p>".$license."</p>",
    12, 2, 6, 2, 16, false, true);
?>
