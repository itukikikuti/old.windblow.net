<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Template.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Game.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/GameFullScreen.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Link.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Text.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Video.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Const.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Space.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Sound.php");

$t = new Template("$shooter2D | ");

new Link($gameDevelop, "/game/develop/", "/image/icon/unity.jpg", 2, 2, false, false);
new Link($shooter2D, "/game/develop/shooter-2d.php", "/image/develop/shooter2D.jpg", 1, 1, false, false);
new Text(
    "<p>$shooter2D</p>",
    7, 1, 2, 2, 26, false, false);

new Game("shooter-2d", 6, 10, 6, 10);
new Space(11, 1, 0, 0);
new GameFullScreen();

?>
