<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Template.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Link.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Text.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Const.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Space.php");

$t = new Template("$installUnity | ");

new Link($gameDevelop, "/game/develop/", "/image/icon/unity.jpg", 2, 2, false, false);
new Link($installUnity, "/game/develop/install.php", "/image/develop/2.jpg", 1, 1, true, false);
new Text(
    "<p>$installUnity</p>",
    8, 1, 2, 2, 26, false, false);
new Link($windblow, "/", "/image/icon/windblow.jpg", 1, 1, false, false);
new Space(10, 1, 0, 0);

$link = "<a href='https://unity3d.com/jp/get-unity/update' target='_blank'>https://unity3d.com/jp/get-unity/update</a>";
$p = array();
$p[] = "まずは".$link."にアクセスして、「インストーラーをダウンロード」をクリックしてくだちい。";
$p[] = "「UnityDownloadAssistant」を実行してくだちい。";
$p[] = "「Next」をクリックくだちい。";
$p[] = "「I accept the なんたらかんたら」にチェックを入れ、「Next」をクリックしてくだちい。";
$p[] = "「Unity」と「Standerd Assets」と「Example Project」にチェックを入れ、「Next」をクリックしてくだちい。";
$p[] = "「Next」をクリックしてくだちい。";
$p[] = "しばらく待ちます。Twitterでも見ててくだちい。";
$p[] = "「Finish」をクリックしてくだちい。";
$p[] = "アカウントを持っている人はサインインしてくだちい。ナンノコッチャわからんって人は「create one」をクリックしてくだちい。";
$p[] = "「Email」「Password」「Username」「Full Name」をちゃんと入力して、「I agree to うんたらかんたら」にチェックを入れ「Create a Unity ID」をクリックしてくだちい。";
$p[] = "こういうメールが来るので、URLをクリックしてくだちい。開いたページで「Email」と「Password」を入力してサインインしてくだちい。";
$p[] = "サインインが終わったら、「Continue」をクリックしてくだちい。";
$p[] = "「Unity Personal」にチェックを入れ「Next」をクリックしてくだちい。";
$p[] = "「I don't use かくかくしかじか」にチェックを入れ「Next」をクリックしてくだちい。";
$p[] = "ここからわアンケートに答えていきます。「Where are you located?」は「Japan」を選んでくだちい。「In what capacity どーたらこーたら」は、とりあえず「Hobbyist」を選んでくだちい。";
$p[] = "「What is your primary role?」は、なんでもいいんですがとりあえず「Indie/Generalist」を選らんどいてくだちい。";
$p[] = "「How would you rate なになに」は、「Beginner」を選んでくだちい。まあどれでもいいんですけどね。";
$p[] = "「Platforms of interest」は、下の方にある「Don't know（訳:ワカラナイヨ）」を選んでくだちい。";
$p[] = "「What type of ほにゃらら」も下の方にある「Don't know（訳:ワカラナイヨ）」を選んでくだちい。";
$p[] = "「OK」をクリックしてくだちい。";
$p[] = "「Start Using Unity」をクリックしてくだちい。";
$p[] = "おっしまい！ちゃん♪ちゃん♪お疲れ様でした。<br><br><br><br>";

new Text(GenerateText($p), 12, 1, 6, 2, 24, false, false);

function GenerateText($p) {
    $text = "";
    for ($i = 0; $i < count($p); $i++) {
        $text = $text."
        <img src='/image/develop/$i.JPG'>
        <p>".$p[$i]."</p>";
    }
    return $text;
}
?>
