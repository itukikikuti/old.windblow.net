<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Template.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Link.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Text.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Video.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/classes/Const.php");

$t = new Template("$stickyCat | ");

new Link($windblow, "/", "/image/icon/windblow.jpg", 4, 4, false, false);
new Link($stickyCat, "/game/sticky-cat.php", "/image/icon/stickycat.jpg", 1, 2, false, false);
new Text(
    "<p>$stickyCat</p>",
    6, 1, 2, 2, 48, false, false);
new Text(
    "<p>Download</p>",
    2, 1, 2, 1, 22, false, false);
new Link("AppStore", "http://appstore.com/stickycatclip", "/image/icon/appstore.jpg", 1, 1, false, true);
new Link("PlayStore", "https://play.google.com/store/apps/details?id=net.windblow.stickycat", "/image/icon/playstore.jpg", 1, 1, false, true);
new Text(
    "<p>8.13.2017 iOS 0.2.1 Tune up!!!</p>
    <p>8.12.2017 Android 0.2.1 Tune up!!!</p>
    <p>8.8.2017 iOS 0.2 New Level and Ending!</p>
    <p>8.5.2017 Android 0.2 New Level and Ending!</p>
    <p>8.2.2017 iOS 0.1.1 First release.</p>
    <p>7.25.2017 Android 0.1 First release.</p>
    <p>10.18.2016 Happy birthday, $stickyCat!!!</p>",
    8, 2, 6, 2, 24, false, true);

$dir = "/image/game/sticky-cat/";
$files = scandir($_SERVER["DOCUMENT_ROOT"].$dir);
sort($files);
foreach ($files as $file) {
    if (is_file($_SERVER["DOCUMENT_ROOT"].$dir.$file)) {
        new Link($file, $dir.$file, $dir.$file, 1, 1, true, false);
    }
}
?>
